package com.example.domain

import kotlinx.coroutines.flow.Flow

interface LocationDataProvider {

    fun listenToGpsUpdates(): Flow<GpsUpdate>

    sealed class GpsUpdate {
        data class GpsData(val latitude: Double, val longitude: Double) : GpsUpdate()
        data class Error(val message: String) : GpsUpdate()
    }
}

