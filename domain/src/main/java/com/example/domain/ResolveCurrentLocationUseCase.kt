package com.example.domain

import kotlinx.coroutines.flow.Flow

class ResolveCurrentLocationUseCase(private val locationDataProvider: LocationDataProvider) {
    fun execute(): Flow<LocationDataProvider.GpsUpdate> {
        return locationDataProvider.listenToGpsUpdates()
    }
}